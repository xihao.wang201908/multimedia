// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "image_ppm.h"
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomImgEcrite[250];
	int nH, nW, nTaille;
    double som=0,som1=0,point;
    double Matri[9][2] ={{-1,1},{0,1},{1,1},{-1,0},{0,0},{1,0},{-1,-1},{0,-1},{1,-1}};
    double Matri_2[9]={0,0,0,0,0,0,0,0,0};
    

    for (int i = 0;i<9;i++){
        Matri_2[i] = exp(-((pow(Matri[i][0],2)+pow(Matri[i][1],2))/(2*1.5*1.5)));
    }
    for (int i = 0;i<9;i++){
        som=som+Matri_2[i];
    }
    for (int i = 0;i<9;i++){
        Matri_2[i] = Matri_2[i]/som;
    }
//已经得到最终的matri

	if (argc != 3) {
		printf("Usage: ImageIn.pgm ImageOut.pgm  \n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomImgEcrite);

	OCTET *ImgIn, *ImgOut;

	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;

	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=0;i<nH;i++){
        for(int j = 0;j<nW;j++){
            if(i==0||j==0||i==nH||j==nW){
                ImgOut[i*nW+j]=ImgIn[i*nW+j];
            }
            else{
            
            ImgOut[i*nW+j] =ImgIn[(i+1)*nW+j-1]*Matri_2[0]+ImgIn[(i+1)*nW+j]*Matri_2[1]+ImgIn[(i+1)*nW+j+1]*Matri_2[2]
                    +ImgIn[(i)*nW+j-1]*Matri_2[3]+ImgIn[i*nW+j]*Matri_2[4]+ImgIn[i*nW+j+1]*Matri_2[5]
                    +ImgIn[(i-1)*nW+j-1]*Matri_2[6]+ImgIn[(i-1)*nW+j]*Matri_2[7]+ImgIn[(i-1)*nW+j+1]*Matri_2[8];
            }
        }
    }

    
    
    

	ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
	free(ImgIn);
	free(ImgOut);

	return 1;
}
