// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomImgEcrite[250];
	int nH, nW, nTaille, g_x,g_y,s_b,s_h,point;

	if (argc != 5) {
		printf("Usage: ImageIn.pgm ImageOut.pgm seuille \n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomImgEcrite);
	sscanf(argv[3], "%d", &s_b);
	sscanf(argv[4], "%d", &s_h);


	OCTET *ImgIn, *ImgOut;

	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;

	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);



    for (int i=1;i<nH-1;i++){
        for(int j = 1;j<nW-1;j++){
            g_x = ImgIn[i * nW + j+1]-ImgIn[i * nW + j-1];
            g_y = ImgIn[(i-1)*nW+j]-ImgIn[(i+1)*nW+j];
            point= sqrt(g_x*g_x+g_y*g_y);
            if(point<=s_b){
                ImgOut[i*nW+j]=0;
            }
            else if (point>=s_h)
            {
                ImgOut[i*nW+j]=255;
            }else{
                ImgOut[i*nW+j]=point;
            }
        }
    }
    for (int i=1;i<nH-1;i++){
        for(int j = 1;j<nW-1;j++){
            if((s_b<ImgOut[i*nW+j]<s_h)&&((ImgOut[i*nW+j-1]==255)||(ImgOut[i*nW+j+1]==255)||(ImgOut[(i-1)*nW+j]==255)||(ImgOut[(i+1)*nW+j]==255)
            ||(ImgOut[(i-1)*nW+j-1]==255)||(ImgOut[(i+1)*nW+j-1]==255)||(ImgOut[(i-1)*nW+j+1]==255)||(ImgOut[(i+1)*nW+j+1]==255)
            )){
                ImgOut[i*nW+j]=255;
            }
            else{
                ImgOut[i*nW+j]=0;
            }
        }
    }
    

	ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
	free(ImgIn);
	free(ImgOut);

	return 1;
}
