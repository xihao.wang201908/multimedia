// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }


   float h[9]={0,0,0,0,0,0,0,0,0};
   float g[9][2]={ {-1,-1}, {-1,0}, {-1,1}, {0,-1},{0, 0},{0, 1},{1, -1},{1, 0},{1,1}};
    float s =0;
    for(int x =0;x<9;x++)
        for (int y=0;y<9;y++)
            {
                h[y]=exp(-((pow(g[x][0],2)+pow(g[x][1],2))/2*pow(0.5,2)));
            }
    for(int z =0;z<9;z++)
            {
                s=s+h[z];
            }
    for(int w =0;w<9;w++)
            {
                h[w]=h[w]/s/9;
            }

 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
      if(i==0 || j==0 || i==nH-1 || j==nW-1){
        ImgOut[i*nW+j]=ImgIn[i*nW+j];
       
      }
      else{
        ImgOut[i*nW+j]=(ImgIn[i*nW+j]*h[4]+ImgIn[i*nW+j+1]*h[7]+ImgIn[i*nW+j-1]*h[1]+ImgIn[(i-1)*nW+j]*h[3]
        +ImgIn[(i+1)*nW+j]*h[5]+ImgIn[(i+1)*nW+j-1]*h[2]
        +ImgIn[(i+1)*nW+j+1]*h[8]+ImgIn[(i-1)*nW+j-1]*h[0]+ImgIn[(i-1)*nW+j+1]*h[6]);
      }

     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}