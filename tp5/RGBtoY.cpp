// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "../tp4/image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nTaille3;

    if (argc != 3)
    {
        printf("Usage: ImageIn.ppm ImageOut.pgm \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    nTaille3 = nTaille*3;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    // int posi[nTaille];
    // for(int i = 0;i<nH;i++){
    //     for(int j = 0;j<nW;j++){
    //         posi[i*nW+j] = 0;
    //     }
    // }

    // for (int k = 0; k < nTaille3; k += 3)
    // {
    //     for (int i = 0; i < nH; i++)
    //     {
    //         for (int j = 0; j < nW; j++)
    //         {
    //             posi[i * nW + j] = ((ImgIn[k] + ImgIn[k + 1] + ImgIn[k + 2]) / 3);
    //         }
    //     }
    // }

    // for(int i = 0;i<nH;i++){
    //     for(int j = 0;j<nW;j++){
    //         ImgOut[i*nW+j] = posi[i*nW+nH];
    //     }
    // }
      for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int pos = 3*(i*nW+j);
            ImgOut[i*nW+j] = (int)((ImgIn[pos] + ImgIn[pos+1] + ImgIn[pos+2])/3);
        }
    }



    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
