// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "../tp4/image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    char cNomImgLue1[250], cNomImgLue2[250];
    int nH, nW, nTaille, som;
    double eqm;

    if (argc != 3)
    {
        printf("Usage: ImageIn_comparer.pgm ImageIn_origin.pgm \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue1);
    sscanf(argv[2], "%s", cNomImgLue2);

    OCTET *ImgIn1, *ImgIn2;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);

    nTaille = nH * nW;

    allocation_tableau(ImgIn1, OCTET, nTaille);
    lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            som+=pow(ImgIn1[i*nW+j]-ImgIn2[i*nW+j],2);
        }
    }
    eqm = som/nTaille;
    printf("%lf\n",eqm);
    free(ImgIn1);
    free(ImgIn2);

    return 1;
}
