// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "../tp4/image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nTaille3, y, cr, cb, composant;

    if (argc != 4)
    {
        printf("Usage: ImageIn.ppm ImageOut.pgm composant(1:y 2:cr 3:cb)\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", composant);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    nTaille3 = nTaille * 3;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    // for (int i = 0; i < nH; i++)
    // {
    //     for (int j = 0; j < nW; j++)
    //     {
    //         int pos = 3 * (i * nW + j);
    //         if (composant == 0)
    //         {
    //             y = (int)((ImgIn[pos] * 0.257 + ImgIn[pos + 1] * 0.504 + ImgIn[pos + 2] * 0.098) + 16);
    //             if (y < 0)
    //             {
    //                 ImgOut[i * nW + j] = 0;
    //             }
    //             else if (y > 255)
    //             {
    //                 ImgOut[i * nW + j] = 255;
    //             }
    //             else
    //             {
    //                 ImgOut[i * nW + j] = y;
    //             }
    //         }
    //         else if (composant == 1)
    //         {
    //             cb = (int)((ImgIn[pos] * (-0.148) + ImgIn[pos + 1] * (-0.291) + ImgIn[pos + 2] * 0.439) + 128);
    //             if (cb < 0)
    //             {
    //                 ImgOut[i * nW + j] = 0;
    //             }
    //             else if (cb > 255)
    //             {
    //                 ImgOut[i * nW + j] = 255;
    //             }
    //             else
    //             {
    //                 ImgOut[i * nW + j] = cb;
    //             }
    //         }
    //         else
    //         {
    //             cr = (int)((ImgIn[pos] * 0.439 + ImgIn[pos + 1] * (-0.368) + ImgIn[pos + 2] * (-0.071)) + 128);
    //             if (cr < 0)
    //             {
    //                 ImgOut[i * nW + j] = 0;
    //             }
    //             else if (cr > 255)
    //             {
    //                 ImgOut[i * nW + j] = 255;
    //             }
    //             else
    //             {
    //                 ImgOut[i * nW + j] = cr;
    //             }
    //         }
    //     }
    // }
     for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int r = ImgIn[3*(i*nW+j)];
            int g = ImgIn[3*(i*nW+j)+1];
            int b = ImgIn[3*(i*nW+j)+2];
            int y = (int)(0 + 0.299*r + 0.587*g + 0.114*b);
            int cb = (int)(128 - 0.1687236*r - 0.331264*g + 0.5*b);
            int cr = (int)(128 + 0.5*r -0.418688*g - 0.081312*b);
            int val;
            if (composant == 1) val = y;
            if (composant == 2) val = cb;
            if (composant == 3) val = cr;
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            ImgOut[i*nW+j] = val;
        }
     }


    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
