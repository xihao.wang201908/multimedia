// threshold_gray.cpp : Seuille une image en niveau de gris selon 3 seuils

#include "../tp4/image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nTaille3, y, cr, cb, composant;

    if (argc != 6)
    {
        printf("Usage: y.pgm cr.pgm cb.pgm ImageOut.ppm )\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", y);
    sscanf(argv[4], "%d", cb);
    sscanf(argv[5], "%d", cr);


    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    nTaille3 = nTaille * 3;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    

     for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int r = ImgIn[3*(i*nW+j)];
            int g = ImgIn[3*(i*nW+j)+1];
            int b = ImgIn[3*(i*nW+j)+2];
            int y = (int)(0 + 0.299*r + 0.587*g + 0.114*b);
            int cb = (int)(128 - 0.1687236*r - 0.331264*g + 0.5*b);
            int cr = (int)(128 + 0.5*r -0.418688*g - 0.081312*b);
            int val;
            if (composant == 1) val = y;
            if (composant == 2) val = cb;
            if (composant == 3) val = cr;
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            ImgOut[i*nW+j] = val;
        }
     }


    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
