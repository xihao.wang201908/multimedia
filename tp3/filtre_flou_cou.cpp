#include "image_ppm.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomImgEcrite[250];
	int nH, nW, nTaille;

	if (argc != 3) {
		printf("Usage: ImageIn.ppm ImageOut.ppm \n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomImgEcrite);

	OCTET *ImgIn, *ImgOut;

	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;
    int nTaille3 = nTaille * 3;

	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);



    for(int i = 0;i<nTaille3;i+=3){
        if((((i/3)%nW)!=0)&&(((i/3)%nW)!=(nW-1))&&((i/3)>nW)&&((i/3)<(nW*(nH-1))))
        {
            ImgOut[i] = (ImgIn[i - 3] + ImgIn[i] + ImgIn[i + 3] + ImgIn[i-3*nW] + ImgIn[i-3*nW - 3] + ImgIn[i-3*nW + 3] + ImgIn[i + 3 * nW] + ImgIn[i + 3 * nW - 3] + ImgIn[i + 3 * nW + 3]) / 9;
            int blue = i+1;
            ImgOut[blue] = (ImgIn[blue - 3] + ImgIn[blue] + ImgIn[blue + 3] + ImgIn[blue-3*nW] + ImgIn[blue-3*nW - 3] + ImgIn[blue-3*nW + 3] + ImgIn[blue + 3 * nW] + ImgIn[blue + 3 * nW - 3] + ImgIn[blue + 3 * nW + 3]) / 9;
            int green = i+2;
            ImgOut[green] = (ImgIn[green - 3] + ImgIn[green] + ImgIn[green + 3] + ImgIn[green-3*nW] + ImgIn[green-3*nW - 3] + ImgIn[green-3*nW + 3] + ImgIn[green + 3 * nW] + ImgIn[green+ 3 * nW - 3] + ImgIn[green + 3 * nW + 3]) / 9;
        }
    }

	ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
	free(ImgIn);
	free(ImgOut);

	return 1;

}