// test_couleur.cpp : Seuille une image en niveau de gris

#include "image_ppm.h"
#include <fstream>
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[]) {
	char cNomImgLue[250], cNomFichierSortie[250];
	int nH, nW, nTaille, S1, S2, S3;
	int histo[256];

	for (int i = 0; i < 256; i++) {
		histo[i] = 0;
	}

	if (argc != 3) {
		printf("Usage: ImageIn.pgm FileOut.txt \n");
		exit(1);
	}

	sscanf(argv[1], "%s", cNomImgLue);
	sscanf(argv[2], "%s", cNomFichierSortie);

	OCTET *ImgIn;

	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;

	allocation_tableau(ImgIn, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

	for (int i = 0; i < nH; i++) {
		for (int j = 0; j < nW; j++) {
			histo[ImgIn[i * nW + j]]++;
		}
	}

	free(ImgIn);

	ofstream outFile;
	outFile.open(cNomFichierSortie);
	for (int i = 0; i < 256; i++) {
		outFile << i << " " << histo[i] << endl;
	}
	outFile.close();
	return 1;
}
